package com.LogosBuh;

    public class Rectangle {

        private double lengthRectangle;
        private double widthRectangle;


        public Rectangle(double lengthRectangle, double widthRectangle) {

            this.lengthRectangle = lengthRectangle;

            this.widthRectangle = widthRectangle;

        }


        public Rectangle() {

            this.lengthRectangle = 3.0;

            this.widthRectangle = 3.0;

        }


        public double getLengthRectangle() {

            return lengthRectangle;

        }


        public void setLengthRectangle(double lengthRectangle) {

            this.lengthRectangle = lengthRectangle;

        }


        public double getWidthRectangle() {

            return widthRectangle;

        }


        public void setWidthRectangle(double widthRectangle) {

            this.widthRectangle = widthRectangle;

        }

        public double theSquare(){

            double squareRectangle = lengthRectangle*widthRectangle;

            return squareRectangle;

        }

        public double thePerymetr() {

            double thePerymetr = ((2*lengthRectangle) + (2*widthRectangle));

            return thePerymetr;

        }
}
